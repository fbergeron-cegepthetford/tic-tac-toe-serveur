const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

/* Adapté de https://codesandbox.io/s/2zx1nj2lqp?file=/server/index.js:1128-1136 par janlukasschroeder*/

const clients = {};

const ajouterClient = socket => {
    console.log("Nouvelle connexion", socket.id);
    clients[socket.id] = socket;
};
const enleverClient = socket => {
    console.log("Client déconnecté", socket.id);
    delete clients[socket.id];
};

io.sockets.on("connection", socket => {
    let id = socket.id;

    ajouterClient(socket);

    // utile ?
    // socket.on("mousemove", data => {
    //     data.id = id;
    //     socket.broadcast.emit("moving", data);
    // });

    socket.on("disconnect", () => {
        enleverClient(socket);
        socket.broadcast.emit("client.deconnecte", id);
    });
});

var joueurs = {},
    en_attente;

function rejoindre(socket, data) {
    // Créer un nouvel objet joueur qu'on ajoute à notre liste de joueurs
    joueurs[socket.id] = {
        // Notre opposant est celui en attente. S'il est null on avisera plus tard
        adversaire: en_attente,

        // Notre couleur
        couleur: "BLEU",

        // Notre socket
        socket: socket,

        //Notre nom
        nom: data.nom
    };

    // Une fois sur deux le joueur en_attente sera null, alors ce sera nous.
    // Par contre, s'il y en a un déjà, alors on devient le joueur 2, donc Rouge
    if (en_attente) {
        joueurs[socket.id].couleur = "ROUGE";
        joueurs[en_attente].adversaire = socket.id;
        en_attente = null;
    } else {
        en_attente = socket.id;
    }
}

// Returns the opponent socket
function monAdversaire(socket) {
    if (!joueurs[socket.id].adversaire) {
        return;
    }

    return joueurs[joueurs[socket.id].adversaire].socket;
}

io.on("connection", function(socket) {

    socket.on("jouer", function(data){
        console.log(data.nom + " veut jouer ")
        rejoindre(socket, data);

        // Si on a un adversaire, on initie la partie
        if (monAdversaire(socket)) {
            socket.emit("partie.debut", {
                couleur: joueurs[socket.id].couleur,
                nom: joueurs[socket.id].nom,
                adversaire: joueurs[monAdversaire(socket).id].nom
            });

            monAdversaire(socket).emit("partie.debut", {
                couleur: joueurs[monAdversaire(socket).id].couleur,
                nom: joueurs[monAdversaire(socket).id].nom,
                adversaire: joueurs[socket.id].nom
            });
        }
    });

    // Lors d'un mouvement, on notifie chaque joueur
    // On se notifie sois-même pour confirmer que l'adversaire est toujours en jeu
    socket.on("coup.jouer", function(data) {
        if (!monAdversaire(socket)) {
            return;
        }

        socket.emit("coup.valide", data);
        monAdversaire(socket).emit("coup.valide", data);
    });

    // Si on se déconnecte, on averti gentiment l'adversaire
    socket.on("disconnect", function() {
        if (monAdversaire(socket)) {
            monAdversaire(socket).emit("adversaire.mort");
        }
    });
});

server.listen(3000, () => {
    console.log('Port 3000 pour conquérir le monde!');
});
